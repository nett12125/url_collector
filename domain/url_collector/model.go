package urlcollector

// PictureURLsResp represents response with pictures URLs.
type PictureURLsResp struct {
	URLs []string `json:"urls"`
}
