package urlcollector

import (
	"context"
	nasapics "pic_handler/domain/url_collector/nasa_pics"
)

// PicturesHandler describes functions available for pictures collecting.
type PicturesHandler interface {
	// Pictures is an endpoint handler which returns image urls.
	Pictures(ctx context.Context, startDate, endDate string) ([]nasapics.APODPictureResp, error)
}
