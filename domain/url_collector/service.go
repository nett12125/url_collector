package urlcollector

import (
	"context"
	"fmt"
	"pic_handler/util"
	"time"
)

// Service provides API endpoints for URL collecting logic.
type Service interface {
	// PictureURLs is an endpoint handler which returns image urls.
	PictureURLs(ctx context.Context, startDateStr, endDateStr string) (*PictureURLsResp, error)
}

type service struct {
	cfg        *util.Config
	picHandler PicturesHandler
}

func NewService(cfg *util.Config, picHandler PicturesHandler) Service {
	return &service{cfg: cfg, picHandler: picHandler}
}

// PictureURLs is an endpoint handler which returns image urls.
func (s *service) PictureURLs(ctx context.Context, startDateStr, endDateStr string) (*PictureURLsResp, error) {
	startDate, err := time.Parse("2006-01-02", startDateStr)
	if err != nil {
		return nil, &util.ErrBadRequest{Msg: fmt.Sprintf("error occured with start_date param: %v", err)}
	}

	endDate, err := time.Parse("2006-01-02", endDateStr)
	if err != nil {
		return nil, &util.ErrBadRequest{Msg: fmt.Sprintf("error occured with end_date param: %v", err)}
	}

	if startDate.After(endDate) {
		return nil, &util.ErrBadRequest{Msg: "start date must not be after end date"}
	}

	picHandlerResp, err := s.picHandler.Pictures(ctx, startDateStr, endDateStr)
	if err != nil {
		return nil, err
	}

	URLs := []string{}
	for i := range picHandlerResp {
		URLs = append(URLs, picHandlerResp[i].URL)
	}
	finalResp := PictureURLsResp{URLs: URLs}

	return &finalResp, nil
}
