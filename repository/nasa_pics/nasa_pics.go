package nasapics

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	nasapics "pic_handler/domain/url_collector/nasa_pics"
	"pic_handler/util"
	"time"
)

const (
	apodURL = "https://api.nasa.gov/planetary/apod"

	// requestTimeout is a context timeout when creating HTTP request.
	requestTimeout = 60 * time.Second
)

type NASAPics struct {
	cfg        *util.Config
	httpClient *http.Client
}

// NewClient create NASA pictures handler for URLCollector.
func NewClient(cfg *util.Config) *NASAPics {
	return &NASAPics{cfg: cfg, httpClient: util.HTTPClient()}
}

// Pictures is an endpoint handler which returns image urls.
func (c *NASAPics) Pictures(ctx context.Context, startDate, endDate string) ([]nasapics.APODPictureResp, error) {
	reqCtx, reqCtxCancel := context.WithTimeout(ctx, requestTimeout)
	defer reqCtxCancel()

	req, err := http.NewRequestWithContext(reqCtx, http.MethodGet, apodURL, nil)
	if err != nil {
		return nil, fmt.Errorf("error while creating %s request to APOD NASA: %w", http.MethodGet, err)
	}

	q := req.URL.Query()
	q.Add("api_key", c.cfg.ApiKey)
	q.Add("start_date", startDate)
	q.Add("end_date", endDate)
	req.URL.RawQuery = q.Encode()

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error while sending %s request to APOD NASA: %w", http.MethodGet, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusTooManyRequests {
		return nil, &util.ErrTooManyRequests{Msg: "limit of maximum requests has been reached"}
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("status code received from response to external APOD NASA endpoint is %d, expected: %d", resp.StatusCode, http.StatusOK)
	}

	var finalResp []nasapics.APODPictureResp
	if err := json.NewDecoder(resp.Body).Decode(&finalResp); err != nil {
		return nil, fmt.Errorf("can't decode JSON from APOD NASA response. err: %w", err)
	}

	return finalResp, nil
}
