package main

import (
	"net/http"
	"pic_handler/api"
	urlcollector "pic_handler/domain/url_collector"

	"github.com/gorilla/mux"
)

func newRouter(srv urlcollector.Service) *mux.Router {
	r := mux.NewRouter()

	urlCollector := api.NewURLCollector(srv)
	r.HandleFunc("/pictures", urlCollector.Pictures).Methods(http.MethodGet)

	return r
}
