package main

import (
	"fmt"
	"net/http"
	urlcollector "pic_handler/domain/url_collector"
	nasapics "pic_handler/repository/nasa_pics"
	"pic_handler/util"
	"time"
)

func main() {
	cfg, err := util.LoadConfig()
	if err != nil {
		fmt.Printf("can't load config: %v", err)
		return
	}

	nasaPics := nasapics.NewClient(cfg)
	urlCollectorService := urlcollector.NewService(cfg, nasaPics)

	server := &http.Server{
		Addr:              fmt.Sprintf("%s:%d", cfg.Host, cfg.Port),
		Handler:           newRouter(urlCollectorService),
		ReadTimeout:       20 * time.Second,
		ReadHeaderTimeout: 15 * time.Second,
		WriteTimeout:      20 * time.Second,
		IdleTimeout:       20 * time.Second,
	}

	fmt.Printf("HTTP Server started on port: %d\n", cfg.Port)
	if err := server.ListenAndServe(); err != nil {
		fmt.Printf("can't start HTTP server: %v", err)
		return
	}

}
