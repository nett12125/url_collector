NAME ?= url-collector
GO_VERSION := 1.17.0
ALPINE_VERSION := 3.14

build:
	CGO_ENABLED=0 \
	go build \
	-v \
	-o $(NAME) .

lint: # linter
	@hash golangci-lint; if [ $$? -ne 0 ]; then \
		echo 'You need to install "golangci-lint" tool: https://github.com/golangci/golangci-lint'; \
		exit 1; \
	fi
	golangci-lint run -v

docker-build: ## builds Docker image with url-collector
	docker build \
	--pull \
	--build-arg GO_VERSION="$(GO_VERSION)" \
	--build-arg ALPINE_VERSION="$(ALPINE_VERSION)" \
	--build-arg NAME="$(NAME)" \
	--tag="$(NAME)" \
	.

download:
	go mod download

test: # unit tests
	go test -v -cover ./...

run:
	DEBUG="false" \
	API_KEY="DEMO_KEY" \
	PORT="8080" \
	HOST="0.0.0.0" \
	go run .

.PHONY: build lint docker-build download test all-tests