# url-collector

By passing query params start_date and end_date you can download list of links to NASA pictures which are uploaded by NASA everyday. For example:

```bash
http://localhost:8080/pictures?start_date=2021-08-15&end_date=2021-08-17
```

There are no integration test and there are some things that could be done better. I wrote this code some times ago and its only to show you in what way I write code.

You need to have installed Go, Docker, Make and Golangci-lint.

## Running url-collector locally:

```bash
make run
```

## Building binary locally:

```bash
make build
```

## Building docker image with url-collector:

```bash
make docker-build
```

## Running unit tests:

```bash
make test
```

## Running linter:

```bash
make lint
```

Other commands you can find in Makefile.