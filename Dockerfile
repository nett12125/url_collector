ARG GO_VERSION
ARG ALPINE_VERSION

# Build
FROM golang:${GO_VERSION}-alpine${ALPINE_VERSION} AS builder

RUN adduser -D -h /tmp/build build
RUN apk --no-cache add \
        make \
        git

USER build
WORKDIR /tmp/build

COPY --chown=build Makefile Makefile
COPY --chown=build go.mod go.mod
COPY --chown=build go.sum go.sum

RUN make download

ARG NAME

COPY --chown=build api api
COPY --chown=build domain domain
COPY --chown=build repository repository
COPY --chown=build util util
COPY --chown=build main.go main.go
COPY --chown=build router.go router.go
RUN make build

# Exec
FROM gcr.io/distroless/base:nonroot

ARG NAME

ENV DEBUG="false"
ENV API_KEY="DEMO_KEY"
ENV PORT="8080"
ENV HOST="0.0.0.0"

COPY --from=builder /tmp/build/${NAME} /usr/local/bin/url-collector

EXPOSE $PORT

CMD ["url-collector"]