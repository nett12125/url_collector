package util

import (
	"crypto/tls"
	"net"
	"net/http"
	"time"
)

// HTTPTimeout is default timeout value in HTTP client config.
const HTTPTimeout = 15 * time.Second

// HTTPClient creates HTTP client.
func HTTPClient() *http.Client {
	return &http.Client{
		Transport: &http.Transport{
			DialContext: (&net.Dialer{
				Timeout:   10 * time.Second,
				KeepAlive: 10 * time.Second,
			}).DialContext,
			TLSHandshakeTimeout:   5 * time.Second,
			ExpectContinueTimeout: 5 * time.Second,
			ResponseHeaderTimeout: HTTPTimeout,
			MaxConnsPerHost:       0,
			TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
		},
		Timeout: HTTPTimeout,
	}
}
