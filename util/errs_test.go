package util

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestErrToHTTPCode(t *testing.T) {
	errBadRequest := &ErrBadRequest{Msg: "bad request msg"}
	assert.Equal(t, ErrToHTTPCode(errBadRequest), http.StatusBadRequest)
	errTooManyRequests := &ErrTooManyRequests{Msg: "too many requests msg"}
	assert.Equal(t, ErrToHTTPCode(errTooManyRequests), http.StatusTooManyRequests)
	otherErr := fmt.Errorf("other error")
	assert.Equal(t, ErrToHTTPCode(otherErr), http.StatusInternalServerError)
}
