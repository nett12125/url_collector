package util

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadConfig(t *testing.T) {
	os.Setenv("DEBUG", "false")
	os.Setenv("API_KEY", "DEMO_KEY")
	os.Setenv("PORT", "8081")
	os.Setenv("HOST", "localhost")

	cfg, err := LoadConfig()
	assert.NoError(t, err)
	assert.Equal(t, cfg.Debug, false)
	assert.Equal(t, cfg.ApiKey, "DEMO_KEY")
	assert.Equal(t, cfg.Port, 8081)
	assert.Equal(t, cfg.Host, "localhost")
}
