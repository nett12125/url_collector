package util

import (
	"errors"
	"net/http"
)

// ErrBadRequest represents an error which appears during bad request (400 code).
type ErrBadRequest struct {
	Msg  string
	Code int
}

// Error returns message of ErrBadRequest.
func (e *ErrBadRequest) Error() string {
	return e.Msg
}

// TooManyRequests represents an error which appears after reaching the limit (429 code).
type ErrTooManyRequests struct {
	Msg  string
	Code int
}

// Error returns message of ErrBadRequest.
func (e *ErrTooManyRequests) Error() string {
	return e.Msg
}

func ErrToHTTPCode(err error) int {
	var errBadRequest *ErrBadRequest
	var errTooManyRequests *ErrTooManyRequests

	if errors.As(err, &errBadRequest) {
		return http.StatusBadRequest
	}

	if errors.As(err, &errTooManyRequests) {
		return http.StatusTooManyRequests
	}

	return http.StatusInternalServerError
}
