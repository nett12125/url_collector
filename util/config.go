package util

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
)

// Config stores all configuration of the application.
type Config struct {
	Debug  bool   `default:"false"`
	ApiKey string `default:"DEMO_KEY"`
	Port   int    `default:"8080"`
	Host   string `default:"0.0.0.0"`
}

// LoadConfig reads environment variables and load them to Config struct.
func LoadConfig() (*Config, error) {
	var cfg Config

	if err := envconfig.Process("", &cfg); err != nil {
		return nil, fmt.Errorf("can't load configuration: %s", err)
	}

	return &cfg, nil
}
