package api

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func writeResponse(w http.ResponseWriter, data interface{}, httpCode int) {
	if err := write(w, data, httpCode); err != nil {
		fmt.Printf("error with writing response: %v", err)
	}
}

func writeErrResponse(w http.ResponseWriter, err error, httpCode int) {
	e := ResponseError{
		Error: err.Error(),
	}

	if err := write(w, e, httpCode); err != nil {
		fmt.Printf("error with writing response: %v", err)
	}
}

func write(w http.ResponseWriter, data interface{}, httpCode int) error {
	jsn, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("can't encode JSON from data[%+v], error: %v", data, err)
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(httpCode)

	if _, err := w.Write(jsn); err != nil {
		return fmt.Errorf("can't write bytes to response writer, error: %v", err)
	}

	return nil
}

type ResponseError struct {
	Error string `json:"error"`
}
