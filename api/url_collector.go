package api

import (
	"fmt"
	"net/http"
	urlcollector "pic_handler/domain/url_collector"
	"pic_handler/util"
)

// URLCollector provides API endpoints for URL collecting logic.
type URLCollector interface {
	// Pictures is an endpoint handler which returns image urls.
	Pictures(w http.ResponseWriter, r *http.Request)
}

type urlCollector struct {
	srv urlcollector.Service
}

func NewURLCollector(srv urlcollector.Service) URLCollector {
	return &urlCollector{srv: srv}
}

// Pictures is an endpoint handler which returns image urls.
func (p *urlCollector) Pictures(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	startDate := query.Get("start_date")
	endDate := query.Get("end_date")

	resp, err := p.srv.PictureURLs(r.Context(), startDate, endDate)
	if err != nil {
		writeErrResponse(w, err, util.ErrToHTTPCode(err))
		fmt.Printf("can't get picture URLs from service[%+v]", err)
		return
	}

	writeResponse(w, resp, http.StatusOK)
}
